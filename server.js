const express = require('express');
const bodyParser= require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({extended: true}));
const MongoClient = require('mongodb').MongoClient;
var objectID = require('mongodb').ObjectID;
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.json());

var db;
MongoClient.connect('mongodb://sajjad:sajjad@ds049211.mlab.com:49211/sajjad', (err, database) => {
  if (err) return console.log(err);
  db = database;

  app.listen(1000, function() {
  console.log('listening on 3000');
});
});



console.log('May Node be with you');
//app.get('/', function(req, res) {
  //res.sendFile(__dirname + '/index.html');
//});
//Insert data
app.post('/quotes', (req, res) => {
   db.collection('quotes').save(req.body, (err, result) => {
    if (err) return console.log(err);

    console.log('saved to database');
    res.redirect('/');
  });
});


//Get Data
app.get('/', (req, res) => {
  db.collection('quotes').find().toArray((err, result) => {
    if (err) return console.log(err);
    // renders index.ejs
    //console.log(result);
    res.render('index.ejs', {quotes: result});
  });
});


//Update Data
app.post('/update', function(req, res){
	var item = {
		name: req.body.name,
		quote: req.body.quote
	};
	var id = req.body.id;
	db.collection('quotes').updateOne({"_id": objectID(id)}, {$set: item}, (err, result) => {
    if (err) return console.log(err);

    console.log('database Updateds');
    res.redirect('/');
  });
});


//Delete Data using Id
app.post('/delete', function(req, res){
	
	var id = req.body.id;
	db.collection('quotes').findOneAndDelete({"_id": objectID(id)}, (err, result) => {
    if (err) return console.log(err);

    console.log('database Deleted');
    res.redirect('/');
  });
});








